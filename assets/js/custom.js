


function openNav2() {
    $("#mySidenav2").addClass("width-menu");
    $("#cd-shadow-layer").css("display", "flex");
    $("body").css("position", "relative");
    $("body").css("overflow", "hidden");
    $("body").css("height", "100vh");
    // $(".position-fixed-overlay").addClass("position-show");
    $(".closebtn2").css("position", "fixed");
   
  
  }
  
  function closeNav2() {
    $("#mySidenav2").removeClass("width-menu");
    $("#cd-shadow-layer").css("display", "none");
    $("body").css("position", "relative");
    $("body").css("overflow", "");
    $("body").css("height", "");
    $(".closebtn2").css("position", "relative");
  
  }


var headertopoption = $(window);
var headTop = $('.navbar-dark');
var captionheader =$(".logo-header")

headertopoption.on('scroll', function () {
    if (headertopoption.scrollTop() > 50) {
        headTop.addClass('fixed-top ');
        captionheader.addClass('header-top');
    } else {
        headTop.removeClass('fixed-top');
        captionheader.removeClass('header-top');
    }
});


$('.btn-search').click(function(){
    $('.serach-text').addClass('d-block');
});

// var headertopoption = $(window);
// var captionheader =$(".logo-header")

// captionheader.on('scroll', function () {
//     if (headertopoption.scrollTop() > 100) {
//         captionheader.addClass('header-top');
//     } else {
//         captionheader.addClass('header-top ');
//     }
// });


$(document).ready(function () {
    $(window).on("resize", function (e) {
        checkScreenSize();
    });

    checkScreenSize();

    function checkScreenSize(){
        var newWindowWidth = $(window).width();
        if (newWindowWidth < 767) {
            $('.menu-logo').attr('src','assets/images/mobile-logo.png');
            $('.btn-search').click(function(){
                $('.serach-text').addClass('d-block');
                $('.close-search').addClass('d-flex');
                
            });

            $('.close-search').click(function(){
                $('.serach-text').removeClass('d-block');
                $('.close-search').removeClass('d-flex');
            });
        }
        else
        {
            
            $('.menu-logo').attr('src','assets/images/logo.svg');
        }
    }

   
});


  

// wow
$(document).ready(function(){

new WOW(
    {  
        mobile:  false,
    }
).init();
    

/// smooth scroll

$('a.smoth-scroll').on("click", function (e) {
    var anchor = $(this);
    $('html, body').stop().animate({
        scrollTop: $(anchor.attr('href')).offset().top - 70
    }, 1500);
    e.preventDefault();
});


// readmore


$("#toggle-read").click(function() {
    var elem = $("#toggle-read").text();
    if (elem == "Read More...") {
      $("#toggle-read").text("Read Less");
      $("#text_hide_show").show();
    } else {
      $("#toggle-read").text("Read More...");
      $("#text_hide_show").hide();
    }
  });


  var newWindowWidth = $(window).width();
        if (newWindowWidth < 767) {

       
          
        }
        else {
            $('.newest-box').click(function(){
        
                // $('.featured-slide').click(function(){
                //     if($('.newest-box').is('active')){
                //         $('.newest-box').removeClass("active");
                //         $('.newest-box').parent().parent().parent().removeClass("active-item");
                //      }
                // });
                
                $('.newest-box').removeClass("active");
                $('.newest-box').parent().parent().parent().removeClass("active-item");
                // $('.owl-item').removeClass("active-none");
                // $('.owl-nav').removeClass("active");
                

                    $(this).toggleClass("active");
                    $(this).parent().parent().parent().toggleClass("active-item");
                    // $('.owl-item').toggleClass("active-none");
                    // $('.owl-nav').toggleClass("active");

                });

                // $('.flip-btn').click(function(){
                //     $(this).parent().parent().hide();
                //     $( "p" ).find( "span" ).css( "color", "red" );
                //     $('.newest-box').find('.flipedbox').css('display','block');
                // });
                // $('.newest-slide .active').click(function(){ 
                //     $('.newest-box').removeClass("active");
                //     $('.newest-box').parent().parent().parent().removeClass("active-item");
                //     $('.owl-item').removeClass("active-none");
                //     $('.owl-nav').toggleClass("active");
                // }); 
        }






$('.flipbox').click(function(){
    $('.flipedbox').removeClass("removed");
    $('.flipedbox').toggleClass("active");

});

$('.arrow-back').click(function(){
    $('.flipedbox').toggleClass("removed");
    $('.flipedbox').RemoveClass("active");
});








// our client


// logo-client


$('.owl-carousel-featured').owlCarousel({
    loop:true,
    margin:0,
    smartSpeed:2000,
    autoplay:true,
    autoplayHoverPause:true,
    dots: false,
    responsiveClass:true,
    nav: true,  
    navText: ['<span class="span-roundcircle left-roundcircle"><i class="fas fa-chevron-left"></i></span>','<span class="span-roundcircle right-roundcircle"><i class="fas fa-chevron-right"></i></span>'],
    responsive:{
        0:{
            items:2,
            nav:true
        },
        768:{
            items:2,
            nav:false
        },

        1000:{
            items:3,
            nav:true
        },
        1025:{
            items:4,
            nav:true,
            loop:true
        }
    }
});



$('.owl-carousel-newest').owlCarousel({
    loop:true,
    margin:0,
    smartSpeed:2000,
    autoplay:false,
    autoplayHoverPause:true,
    dots: false,
    responsiveClass:true,
    nav: true,  
    navText: ['<span class="span-roundcircle left-roundcircle"><i class="fas fa-chevron-left"></i></span>','<span class="span-roundcircle right-roundcircle"><i class="fas fa-chevron-right"></i></span>'],
    responsive:{
        0:{
            items:2,
            nav:true
        },
        768:{
            items:2,
            nav:true,
        },

        1000:{
            items:3,
            nav:true
        },
        1025:{
            items:4,
            nav:true,
            loop:true
        }
    }
});


// our service

$('.owl-carousel-hero').owlCarousel({
    loop:true,
    margin:0,
    smartSpeed:2000,
    autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    dots: true,
    responsiveClass:true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    responsiveClass:true,
    mouseDrag: false,
    touchDrag: true,
    navText: ['<span class="span-roundcircle left-roundcircle"><i class="fas fa-chevron-left"></i></span>','<span class="span-roundcircle right-roundcircle"><i class="fas fa-chevron-right"></i></span>'],
    responsive:{
        0:{
            items:1,
            nav:false
        },
        768:{
            items:1,
            nav:true
        },

        1000:{
            items:1,
            nav:true
        },
        1025:{
            items:1,
            nav:true,
            loop:true
        }
    }
});




});